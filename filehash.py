import os
import hashlib

from collections import Counter

# 使用python3.8及以上可以用此方法，写法更简洁。


def file_hash(file_path: str, hash_method) -> str:
    if os.path.isdir:
        file_hashs = []
        listdir = os.listdir(file_path)
        for file in listdir:
            if not os.path.isfile(file_path + file):
                print('文件不存在。')
                return ''
            h = hash_method()
            with open(file_path + file, 'rb') as f:
                while b := f.read(8192):
                    h.update(b)
                    file_hashs.append(h.hexdigest())
        return file_hashs
    else:
        if not os.path.isfile(file_path):
            print('文件不存在。')
            return ''
        h = hash_method()
        with open(file_path, 'rb') as f:
            while b := f.read(8192):
                h.update(b)
        return h.hexdigest()


def str_hash(content: str, hash_method, encoding: str = 'UTF-8') -> str:
    return hash_method(content.encode(encoding)).hexdigest()


def file_md5(file_path: str) -> str:
    return file_hash(file_path, hashlib.md5)


def file_sha256(file_path: str) -> str:
    return file_hash(file_path, hashlib.sha256)


def file_sha512(file_path: str) -> str:
    return file_hash(file_path, hashlib.sha512)


def file_sha384(file_path: str) -> str:
    return file_hash(file_path, hashlib.sha384)


def file_sha1(file_path: str) -> str:
    return file_hash(file_path, hashlib.sha1)


def file_sha224(file_path: str) -> str:
    return file_hash(file_path, hashlib.sha224)


def str_md5(content: str, encoding: str = 'UTF-8') -> str:
    return str_hash(content, hashlib.md5, encoding)


def str_sha256(content: str, encoding: str = 'UTF-8') -> str:
    return str_hash(content, hashlib.sha256, encoding)


def str_sha512(content: str, encoding: str = 'UTF-8') -> str:
    return str_hash(content, hashlib.sha512, encoding)


def str_sha384(content: str, encoding: str = 'UTF-8') -> str:
    return str_hash(content, hashlib.sha384, encoding)


def str_sha1(content: str, encoding: str = 'UTF-8') -> str:
    return str_hash(content, hashlib.sha1, encoding)


def str_sha224(content: str, encoding: str = 'UTF-8') -> str:
    return str_hash(content, hashlib.sha224, encoding)


if __name__ == '__main__':
    print(os.cpu_count())
    # hash = file_sha256('C:/Users/z1059/PycharmProjects/generate_nft/output/edition 10000/images/')
    # b = dict(Counter(hash))
    #
    # print({key: value for key, value in b.items() if value > 1})